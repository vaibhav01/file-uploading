<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function fileUpload()
    {
        return view('fileUpload.fileUpload');
    }
    public function fileUploadPost(Request $request)
    {
        $request->validate([
            'file' => 'required',
        ]);

        $fileName = time() . '.' . request()->file->getClientOriginalExtension();

        request()->file->move(public_path('files'), $fileName);

        return response()->json(['success' => 'You have successfully upload file.']);
    }
    // public function dashboard()
    // {
    //     return view('dashboard.dashboard');
    // }
    // public function index()
    // {
    //     dd('hello this is index page');
    // }
    // public function upload()
    // {
    //    return view('dashboard.upload');
    // }
}
